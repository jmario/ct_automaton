import javafx.collections.ObservableList
import javafx.scene.Node

import scala.collection.mutable.ArrayBuffer
import scalafx.scene.paint.Color
import scalafx.scene.shape.{Circle, Line}
import scalafx.scene.text.{FontWeight, Font, Text}
/**
 * Created by Light on 30-Jan-16.
 */
class CanvasDrawer(automata: DFA, content:ObservableList[Node]) {

  def DrawState(_state:State): Unit ={
    val stateBody: Circle = new Circle(){
      fill = if(_state.Acceptance) Color.LimeGreen else if(automata.q0 == _state) Color.Yellow else Color.LightSkyBlue
      stroke = Color.DarkSlateGray
      radius = 15
      centerX.value = _state.Position.X
      centerY.value = _state.Position.Y
    }
    val stateLabel:Text = DrawStateLabel(_state)

    content.add(stateBody)
    content.add(stateLabel)

  }

  def DrawStateLabel(_state:State): Text ={
    val offset:Double = 3
    val label = new Text(){
      fill = Color.Black
      text = _state.Name
      layoutX.value = _state.Position.X - offset*2
      layoutY.value = _state.Position.Y + offset
    }
    label
  }

  def DrawStateLoop(_state:State):Text = {
    val offset:Double = 3
    val label = new Text(){
      fill = Color.Black
      text = "\u27F3"
      layoutX.value = _state.Position.X - offset * 3.7
      layoutY.value = _state.Position.Y - offset * 2.3
    }
    label.setFont(Font.font("Verdana", FontWeight.Bold, 37))
    label
  }

  def DrawStateLoopLabel(symbol:String, _x:Double, _y:Double):Text = {
    val offset:Double = 3
    val label = new Text(){
      fill = Color.Black
      text = symbol
      layoutX.value = _x + 10
      layoutY.value = _y - 25
    }
    label
  }


  def DrawTransition(transition:Transition): Unit ={
    if(!transition.Origin.equals(transition.Destiny)) {
      val transitionEdge = new Line() {
        startX.value = transition.Origin.Position.X
        startY.value = transition.Origin.Position.Y + 15
        endX.value = transition.Destiny.Position.X
        endY.value = transition.Destiny.Position.Y - 15
        stroke = Color.Black
      }

      transitionEdge.toBack()
      val transitionLabel: Text = DrawTransitionLabel(transition)
      content.add(transitionLabel)
      content.add(transitionEdge)
      return
    }

    val stateLoop:Text = DrawStateLoop(transition.Origin)
    val stateLoopLabel:Text = DrawStateLoopLabel(transition.Name, stateLoop.getLayoutX, stateLoop.getLayoutY)
    content.add(stateLoopLabel)
    content.add(stateLoop)
    stateLoop.toBack()
  }

  def DrawTransitionLabel(transition:Transition): Text ={
    val transitionLabel = new Text(){
      fill = Color.Purple
      text = transition.Name
      layoutX.value = (transition.Destiny.Position.X + transition.Origin.Position.X)/2 + 9
      layoutY.value = (transition.Destiny.Position.Y + transition.Origin.Position.Y)/2 + 9
    }
    transitionLabel
  }

  def reDraw(componentList:ArrayBuffer[Node]): Unit ={
    automata._states.keys.foreach{
                                  i => automata._states(i).transitions.keys.foreach{
                                                                                    j => DrawTransition(automata._states(i).transitions(j))
                                                                                   }
                                  }
    automata._states.keys.foreach{i => DrawState(automata._states(i))}

    reDrawActionComponents(componentList)
  }

  def reDrawActionComponents(componentList:ArrayBuffer[Node]): Unit = {
    for(button <- componentList){ content.add(button) }
  }
}
