/**
 * Created by Light on 30-Jan-16.
 */

import scala.collection.mutable

class State(_stateName: String, _position: Point) {

  var Name:String = _stateName
  var Position:Point = _position
  var Acceptance:Boolean = false
  var transitions = mutable.HashMap.empty[String, Transition]

}

