
/**
 * Created by Light on 30-Jan-16.
 */

import javafx.scene.Node

import scala.collection.mutable.ArrayBuffer
import scalafx.Includes.jfxMouseEvent2sfx
import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.control.{Button, TextInputDialog}
import scalafx.scene.input.MouseEvent
import scalafx.scene.paint.Color
import scalafx.scene.shape.Rectangle
import scalafx.scene.text.{Text, FontWeight, Font}

object Main extends JFXApp {
  val obj = new My
  obj.print()

  val automata = new DFA()
  var actionComponentList = ArrayBuffer.empty[Node]
  private val _prefheight:Double = 40
  private val _prefwidth:Double = 200

  stage = new JFXApp.PrimaryStage {
    title.value = "Computer Theory: Automaton"
    width = 1020
    height = 700
    scene = new Scene {
      val canvas = new CanvasDrawer(automata, content)
      val alert = new AlertToasters(stage)

      fill = Color.White
      var actionPanel = new Rectangle {
        x = 700
        y = 0
        width = 300
        height = 700
        fill = Color.Gray
      }
      var statePanel = new Rectangle {
        x = 725
        y = 50
        width = 250
        height = 220
        fill = Color.WhiteSmoke
      }
      var transitionPanel = new Rectangle {
        x = 725
        y = 300
        width = 250
        height = 150
        fill = Color.WhiteSmoke
      }

      val stateLabel = new Text(){
        fill = Color.DarkSlateGray
        text = "State Actions \u27AA"
        layoutX.value = 735
        layoutY.value = 70
      }

      val transitionLabel = new Text(){
        fill = Color.DarkOliveGreen
        text = "Transition Actions \u27AA"
        layoutX.value = 735
        layoutY.value = 320
      }


      /*ADD STATE*/
        handleEvent(MouseEvent.MouseClicked) {
          a: MouseEvent => {
            val dialog = new TextInputDialog(defaultValue = "") {
              initOwner(stage)
              title = "Add State"
              contentText = "Enter a var-like name:"
            }
            val result = dialog.showAndWait()
            result match {
              case Some(stateName) =>
                if(!automata._states.contains(stateName)){
                  val addedstate =  automata.addState(stateName, new Point(a.sceneX, a.sceneY))
                  canvas.DrawState(addedstate)
                }

              case None => print("")
            }
          }
        }
      /*END ADD STATE*/

      /*SET INITIAL STATE*/
        val _setq0 = new Button(" \u272E Set q0") {
          layoutX = 740
          layoutY = 100
          prefHeight = _prefheight
          textFill = Color.DarkBlue

          handleEvent(MouseEvent.MouseClicked) {
            a: MouseEvent => {
              val dialog = new TextInputDialog(defaultValue = "") {
                initOwner(stage)
                title = "Set q0"
                contentText = "Initial state-name:"
              }
              val result = dialog.showAndWait()
              result match {
                case Some(stateName) => val res = automata.setInitialState(stateName)
                                        content.clear()
                                        canvas.reDraw(actionComponentList)
                case None => print("")
              }
            }
          }
        }
      /*END SET INITIAL STATE*/

      /*MARK STATE AS ACCEPTED*/
        val _markStateAsAccepted = new Button(" \u2714 Set State as Accepted") {
          layoutX = 740
          layoutY = 160
          prefHeight = _prefheight
          prefWidth = _prefwidth
          textFill = Color.DarkGreen

          handleEvent(MouseEvent.MouseClicked) {
            a: MouseEvent => {
              val dialog = new TextInputDialog(defaultValue = "") {
                initOwner(stage)
                title = "Accepted State"
                contentText = "State to be Accepted:"
              }
              val result = dialog.showAndWait()
              result match {
                case Some(stateName) => val res = automata.markAsAcceptanceState(stateName)
                                        content.clear()
                                        canvas.reDraw(actionComponentList)
                case None => print("")
              }
            }
          }
        }
      /*END MARK STATE AS ACCEPTED*/

      /*EDIT STATE*/
        var stateToBeEdited:String = ""
        val _editState = new Button(" \u270E Edit State") {
          layoutX = 840
          layoutY = 100
          prefHeight = _prefheight
          textFill = Color.DarkOrange

          handleEvent(MouseEvent.MouseClicked) {
            a: MouseEvent => {
              val dialog = new TextInputDialog(defaultValue = "") {
                initOwner(stage)
                title = "Edit State"
                contentText = "State to be edited:"
              }
              val result = dialog.showAndWait()
              result match {
                case Some(stateName) => stateToBeEdited = stateName
                  content.clear()
                  canvas.reDraw(actionComponentList)
                case None => print("")
              }

              if(!stateToBeEdited.isEmpty) {
                val dialog2 = new TextInputDialog(defaultValue = "") {
                  initOwner(stage)
                  title = "Edit State"
                  contentText = "New state name:"
                }
                val result2 = dialog2.showAndWait()
                result2 match {
                  case Some(newStateName) => val res = automata.editState(stateToBeEdited, newStateName)
                    content.clear()
                    canvas.reDraw(actionComponentList)
                  case None => print("")
                }
              }
            }
          }
        }
      /*END EDIT STATE*/

      /*REMOVE STATE*/
        val _removeState = new Button(" \u274E Remove State") {
          layoutX = 740
          layoutY = 220
          prefHeight = _prefheight
          prefWidth = _prefwidth
          textFill = Color.Crimson

          handleEvent(MouseEvent.MouseClicked) {
            a: MouseEvent => {
              val dialog = new TextInputDialog(defaultValue = "") {
                initOwner(stage)
                title = "Remove State"
                contentText = "Enter a var-like name:"
              }
              val result = dialog.showAndWait()
              result match {
                case Some(stateName) => val res = automata.deleteState(stateName)
                                        content.clear()
                                        canvas.reDraw(actionComponentList)
                case None => print("")
              }
            }
          }
        }
      /*END REMOVE STATE*/

      /*ADD TRANSITION*/
        var fromState: String = ""
        var toState: String = ""
        val _addTransition = new Button(" \u271A Add Transition") {
          layoutX = 740
          layoutY = 350
          prefHeight = _prefheight
          prefWidth = _prefwidth
          textFill = Color.DarkBlue

          handleEvent(MouseEvent.MouseClicked) {
            a: MouseEvent => {
              val dialog = new TextInputDialog(defaultValue = "") {
                initOwner(stage)
                title = "Add origin"
                contentText = "From:"
              }
              val resultTo = dialog.showAndWait()
              resultTo match {
                case Some(_fromState) => fromState = _fromState
                case None => print("")
              }

              if (!fromState.isEmpty) {
                val dialog2 = new TextInputDialog(defaultValue = "") {
                  initOwner(stage)
                  title = "Add destiny"
                  contentText = "To:"
                }
                val resultFrom = dialog2.showAndWait()
                resultFrom match {
                  case Some(_toState) => toState = _toState
                  case None => print("")
                }
              }

              if (!toState.isEmpty) {
                val dialog3 = new TextInputDialog(defaultValue = "") {
                  initOwner(stage)
                  title = "Enter Value"
                  contentText = "=>"
                }
                val symbol = dialog3.showAndWait()
                symbol match {
                  case Some(transitionSymbol) =>
                    val originState = if(automata._states.contains(fromState)) automata._states(fromState) else null
                    val destinystate = if(automata._states.contains(toState)) automata._states(toState) else null
                    if(originState != null && destinystate != null) {
                      val addedTransition = automata.addTransition(fromState, toState, transitionSymbol)
                      canvas.DrawTransition(addedTransition)
                    }else{
                      alert.errorNoSuchState("Invalid State Name", "Invalid State Name", fromState, toState)
                    }

                  case None => print("")
                }
              }
            }
          }
        }
      /*END ADD TRANSITION*/

      /*REMOVE TRANSITION*/
        var RfromState: String = ""
        var RtoState: String = ""
        val _removeTransition = new Button(" \u274E Remove Transition") {
          layoutX = 740
          layoutY = 400
          prefHeight = _prefheight
          prefWidth = _prefwidth
          textFill = Color.Crimson

          handleEvent(MouseEvent.MouseClicked) {
            a: MouseEvent => {
              val dialog = new TextInputDialog(defaultValue = "") {
                initOwner(stage)
                title = "Input origin"
                contentText = "From:"
              }
              val resultTo = dialog.showAndWait()
              resultTo match {
                case Some(_fromState) => RfromState = _fromState
                case None => print("")
              }

              if (!RfromState.isEmpty) {
                val dialog2 = new TextInputDialog(defaultValue = "") {
                  initOwner(stage)
                  title = "Input destiny"
                  contentText = "To:"
                }
                val resultFrom = dialog2.showAndWait()
                resultFrom match {
                  case Some(_toState) => RtoState = _toState
                  case None => print("")
                }
              }

              if (!RtoState.isEmpty) {
                val dialog3 = new TextInputDialog(defaultValue = "") {
                  initOwner(stage)
                  title = "Input Value"
                  contentText = "=>"
                }
                val symbol = dialog3.showAndWait()
                symbol match {
                  case Some(transitionSymbol) =>
                    val originState = if(automata._states.contains(RfromState)) automata._states(RfromState) else null
                    val destinystate = if(automata._states.contains(RtoState)) automata._states(RtoState) else null
                    if(originState != null && destinystate != null) {
                      val res = automata.deleteTransition(RfromState, RtoState, transitionSymbol)
                      content.clear()
                      canvas.reDraw(actionComponentList)
                    }else{
                      alert.errorNoSuchState("Invalid State Name", "Invalid State Name", RfromState, RtoState)
                    }

                  case None => print("")
                }
              }
            }
          }
        }
      /*END REMOVE TRANSITION*/

      /*EVALUATE EXPRESSION*/
        val _evaluate = new Button(" \u2705 Evaluate") {
          layoutX = 750
          layoutY = 500
          prefWidth = 200
          prefHeight = _prefheight
          textFill = Color.ForestGreen

          handleEvent(MouseEvent.MouseClicked) {
            a: MouseEvent => {
              val dialog = new TextInputDialog(defaultValue = "") {
                initOwner(stage)
                title = "Enter Expression"
                contentText = "String here =>"
              }
              val result = dialog.showAndWait()
              result match {
                case Some(expression) => val res = automata.evaluateExpression(expression)
                  alert.errorEvaluate("Expression Evaluation", "Expression Evaluation", "Expression Accepted", "Expression Rejected", res)

                case None => print("")
              }
            }
          }
        }
      /*END EVALUATE EXPRESSION*/
      _evaluate.setFont(Font.font("Verdana", FontWeight.Bold, 20))
      _removeState.setFont(Font.font("Calibri", FontWeight.Bold, 15))
      _addTransition.setFont(Font.font("Calibri", FontWeight.Bold, 15))
      _markStateAsAccepted.setFont(Font.font("Calibri", FontWeight.Bold, 15))
      _editState.setFont(Font.font("Calibri", FontWeight.Bold, 15))
      _setq0.setFont(Font.font("Calibri", FontWeight.Bold, 15))
      _removeTransition.setFont(Font.font("Calibri", FontWeight.Bold, 15))
      stateLabel.setFont(Font.font("Calibri", FontWeight.Bold, 17))
      transitionLabel.setFont(Font.font("Calibri", FontWeight.Bold, 17))

      actionComponentList += actionPanel
      actionComponentList += statePanel
      actionComponentList += transitionPanel
      actionComponentList += transitionLabel
      actionComponentList += stateLabel
      actionComponentList += _markStateAsAccepted
      actionComponentList += _editState
      actionComponentList += _setq0
      actionComponentList += _removeState
      actionComponentList += _addTransition
      actionComponentList += _removeTransition
      actionComponentList += _evaluate
      canvas.reDrawActionComponents(actionComponentList)

    } /*END SCENE*/

    def isValidPosition(_x: Double, _y: Double): Boolean = {
      true
    }
  }
}