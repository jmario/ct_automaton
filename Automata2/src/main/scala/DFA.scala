/**
 * Created by Light on 30-Jan-16.
 */
/**
 * Created by Light on 30-Jan-16.
 */

import scala.collection.mutable

class DFA {
  var _states = mutable.HashMap.empty[String, State]
  var q0:State = null

  def addState(_stateName: String, _position:Point): State ={
    if(_states.contains(_stateName))
      return null

    _states += (_stateName -> new State(_stateName, _position))
    _states(_stateName)
  }

  def deleteState (_stateName: String): Boolean ={
    if(!_states.contains(_stateName))
      return false

    val toDelete = _states(_stateName)
    _states.keys.foreach{
      i => _states(i).transitions.keys.foreach {
        j => if(_states(i).transitions(j).Destiny.equals(toDelete) || _states(i).transitions(j).Origin.equals(toDelete)){
                _states(i).transitions.remove(j)
              }
      }
    }
    _states.remove(_stateName)
    if(_states.isEmpty) q0 = null

    true
  }

  def setInitialState(_stateName: String): Boolean ={
    if(!_states.contains(_stateName))
      return false

    q0 = _states(_stateName)
    true
  }

  def markAsAcceptanceState(_stateName: String): Boolean ={
    if(!_states.contains(_stateName))
      return false

    _states(_stateName).Acceptance = true
    true
  }

  def addTransition(_originStateName:String, _destinyStateName:String, __transitionSymbol:String): Transition ={
    if(!_states.contains(_originStateName) || !_states.contains(_destinyStateName))
      return null

    val originState = _states(_originStateName)
    val destinyState = _states(_destinyStateName)

    if(originState.transitions.contains(__transitionSymbol))
      return null

    val newTransition = new Transition(__transitionSymbol, originState, destinyState)
    originState.transitions += (__transitionSymbol -> newTransition)
    newTransition
  }

  def deleteTransition(_stateName:String,_toStateName:String, _transitionSymbol:String): Boolean ={
    if(!_states.contains(_stateName) || !_states.contains(_toStateName))
      return false

    val state:State = _states(_stateName)
    if(!state.transitions.contains(_transitionSymbol))
      return false

    val tranToDelete = state.transitions(_transitionSymbol)

    if(!tranToDelete.Destiny.equals(_states(_toStateName)))
      return false

    state.transitions.remove(_transitionSymbol)
    true
  }

  def editState(_stateName:String, _stateNewName:String): Boolean ={
    if(!_states.contains(_stateName))
      return false

    if(_states.contains(_stateNewName))
      return false

    val state = _states(_stateName)
    _states.remove(_stateName)
    state.Name = _stateNewName
    _states += (_stateNewName -> state)
    true
  }

  def evaluateExpression(_expression:String): Boolean ={

    var currentState = q0

    for(symbol <- _expression){
      if(currentState == null) return false

      if(currentState.transitions.isEmpty) return false

      if(currentState.transitions.contains(symbol.toString))
        currentState = currentState.transitions(symbol.toString).Destiny
      else
        return false
    }

    currentState.Acceptance
  }
}
