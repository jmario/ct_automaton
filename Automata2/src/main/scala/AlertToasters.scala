import scalafx.application.JFXApp
import scalafx.scene.control.Alert
import scalafx.scene.control.Alert.AlertType

/**
 * Created by Light on 31-Jan-16.
 */
class AlertToasters(stage:JFXApp.PrimaryStage) {

  def errorEvaluate(_title:String, _header: String, _response:String, _response2:String, _condition: Boolean): Unit ={
    val _alertType: AlertType = if (_condition) AlertType.Confirmation else AlertType.Error
    new Alert(_alertType) {
      initOwner(stage)
      title = _title
      headerText = _header
      contentText = if(_condition) _response else _response2

    }.showAndWait()
  }

  def errorNoSuchState(_title:String, _header: String, _state1:String, _state2:String): Unit ={
    new Alert(AlertType.Error) {
      initOwner(stage)
      title = _title
      headerText = _header
      contentText = "Either " +_state1 + " or " + _state2 + " dont exist"

    }.showAndWait()
  }

}
