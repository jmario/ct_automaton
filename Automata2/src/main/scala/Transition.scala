/**
 * Created by Light on 30-Jan-16.
 */
class Transition(_transitionName:String, _origin:State, _destiny:State) {
  var Name: String = _transitionName
  var Origin:State = _origin
  var Destiny:State = _destiny
}
